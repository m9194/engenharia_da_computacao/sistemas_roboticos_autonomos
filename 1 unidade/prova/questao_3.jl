using Polynomials

function radius_of_curvature(x::Polynomial, y::Polynomial, λ::Float64)::Float64
    δx = derivative(x)

    δ²x = derivative(δx)

    δy = derivative(y)

    δ²y = derivative(δy)


    numerator = δx * δx + δy * δy

    numerator_value = numerator(λ)^(3 / 2)

    denominator = δ²y * δx - δ²x * δy

    denominator_value = denominator(λ)

    return numerator_value / denominator_value

end



x = Polynomial([0, 10], "λ")

y = Polynomial([0, 0, 20, -10], "λ")

println(x)

println(y)

println("numerador: ($numerator)^(3/2)")
println("denominador: $denominator")

println("δx: $δx")
println("δ²x: $δ²x")
println("δy: $δy")
println("δ²y: $δ²y")


initial_value_radius = radius_of_curvature(x, y, 0.0)

middle_value_radius = radius_of_curvature(x, y, 0.5)

final_value_radius = radius_of_curvature(x, y, 1.0)


println(" r(0): $initial_value_radius  r(0.5) $middle_value_radius  r(1.0) $final_value_radius ")


import numpy as np
from matplotlib import pyplot
from scipy.signal import wiener
from mpl_toolkits.mplot3d import Axes3D


class EulerIntegrate:

    def __init__(self) -> None:
        self.last_value = 0


def main():

    data = np.loadtxt('data.csv', delimiter=',')

    # data= np.round(data,decimals= 2)

    shape_without_time_column = data.shape[0], data.shape[1]-1
    first_integrate = np.zeros(shape=shape_without_time_column)
    second_integrate = np.zeros(shape=shape_without_time_column)

    time_in_milliseconds = data[:, -1]

    for index in range(1, data.shape[0]):
        sample = data[index, :-1]
        dt = time_in_milliseconds[index] - time_in_milliseconds[index-1]
       
        first_integrate[index] = sample*dt + first_integrate[index-1]


        second_integrate[index] = first_integrate[index] * dt + second_integrate[index-1]

    np.savetxt('first_integrate.csv', first_integrate, delimiter=',')
    np.savetxt('second_integrate.csv', second_integrate, delimiter=',')


    ax = pyplot.axes(projection='3d')

    ax.plot(second_integrate[:,0],second_integrate[:,1],second_integrate[:,2])

    pyplot.figure(2)

    pyplot.plot(second_integrate[:,0],second_integrate[:,1])

    pyplot.figure(3)
    ax = pyplot.axes(projection='3d')

    ax.plot(second_integrate[:,3],second_integrate[:,4],second_integrate[:,5])
    
    pyplot.figure(4)

    pyplot.plot(time_in_milliseconds,second_integrate[:,4])

    pyplot.show()


if __name__ == '__main__':
    main()
